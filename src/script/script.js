// DOCUMENT CALLS
const mainGameArea = document.querySelector('#gameMainContainer');
const modal = document.getElementById('modal-inicia');
const innerModal = document.getElementById('modal');
const music = new Audio('./src/assets/songs/theme-music.mp3');



const nicknames = [];
// ------------------------------------------------------------------
// Grid Configuration
const gridX = 7;
const gridY = 6;
let lastElements = [];
let arrPlay1 = [];
let arrPlay2 = [];

const gridCreator = (sizeY, sizeX) => {
    let gameGridContainer = document.createElement('div');
    gameGridContainer.classList.add('gameGrids');

    for (let counter = 1; counter <= ( sizeY * sizeX ); counter++) {
        let newGrid = document.createElement('div');

        newGrid.classList.add('emptyGrid');
        newGrid.setAttribute('id', `grid${counter}`);

        gameGridContainer.appendChild(newGrid);

        if (counter <= 42 && counter >= 36) {lastElements.push(newGrid.id);}
    };

    mainGameArea.appendChild(gameGridContainer);

    for (let counter = 0; counter <= sizeX - 1; counter++) {
        let section = document.createElement('div');

        section.setAttribute('id', `section${counter}`);
        section.classList.add('gameSection');

        section.addEventListener('click', function(){
            if (!gameCondition) {return};

            let lastGrid = document.querySelector(`#${lastElements[counter]}`);

            if (lastGrid === null) {return};

            let nextLastGrid;
            if (lastElements[counter].length === 6) {
                nextLastGrid = parseInt(lastElements[counter][lastElements[counter].length - 2] + lastElements[counter][lastElements[counter].length - 1]) - 7;
            } else {
                nextLastGrid = parseInt(lastElements[counter][lastElements[counter].length - 1]) - 7;
            }

            lastGrid.classList.remove('emptyGrid');
            
            if (actualTurn === 1) {
                lastGrid.classList.add('player1');
            } else {
                lastGrid.classList.add('player2');
            }
            
            lastElements[counter] = `grid${nextLastGrid}`;


            let lastGridCoor = 7;
            if (lastElements[counter].length === 6) {
                lastGridCoor += parseInt(lastElements[counter][lastElements[counter].length - 2] + lastElements[counter][lastElements[counter].length - 1]);
            } else {
                lastGridCoor += parseInt(lastElements[counter][lastElements[counter].length - 1]);
            }

            let sectionId = (parseInt(section.id[7]) + 1).toString();
            if (lastGridCoor <= 7) {
                lastGridCoor = sectionId + '1';
            } else if (lastGridCoor <= 14) {
                lastGridCoor = sectionId + '2';
            } else if (lastGridCoor <= 21) {
                lastGridCoor = sectionId + '3';
            } else if (lastGridCoor <= 28) {
                lastGridCoor = sectionId + '4'
            } else if (lastGridCoor <= 35) {
                lastGridCoor = sectionId + '5';
            } else {
                lastGridCoor = sectionId + '6';
            };

            if (actualTurn === 2) {
                arrPlay2.push(lastGridCoor);
            } else {
                arrPlay1.push(lastGridCoor);
            }
            location
            condVitoria();
            turnSelection(actualTurn);
        });

        gameGridContainer.appendChild(section);
    }
}; gridCreator(gridY, gridX);
// ------------------------------------------------------------------
// AVATAR SELECTION
const avatarOptions = document.querySelectorAll(".pokeImg");
let avatarOne;
let avatarTwo;

const playersAvatar = () => {
    for (let counter = 0; counter < 4; counter++) {
        avatarOptions[counter].addEventListener('click', function(E){
            for (let counter2 = 0; counter2 < 4; counter2++) {
                avatarOptions[counter2].removeAttribute('id');
            }

            let selectedPoke = E.target;
            
            if (selectedPoke.id === 'playerOneAvatar') {
                selectedPoke.removeAttribute('id');
                avatarOne = selectedPoke;
            } else {
                selectedPoke.setAttribute('id', 'playerOneAvatar');
                avatarOne = selectedPoke;
            };
        });
    };

    for (let counter = 4; counter < 8; counter++) {
        avatarOptions[counter].addEventListener('click', function(E){
            for (let counter2 = 4; counter2 < 8; counter2++) {
                avatarOptions[counter2].removeAttribute('id');
            }

            let selectedPoke = E.target;
            
            if (selectedPoke.id === 'playerTwoAvatar') {
                selectedPoke.removeAttribute('id');
                avatarTwo = selectedPoke;
            } else {
                selectedPoke.setAttribute('id', 'playerTwoAvatar');
                avatarTwo = selectedPoke;
            }
        });
    };
}; playersAvatar();
// --------------------------------------------------------------------
// GAMEPLAY CONFIGURATION
let currentPlayer;
let gameCondition = false;
let playerOneSprites;
let playerTwoSprites;
let turnCount = document.querySelector('#playerScore'); 

let gameFPS = setInterval(() => {   
    turnCount.innerHTML = `Current Turn:<br>>>> ${currentPlayer} <<<`;


    playerOneSprites = document.querySelectorAll('.player1');
    playerTwoSprites = document.querySelectorAll('.player2');
    for (let counter = 0; counter < playerOneSprites.length; counter++) {
        let currentElement = playerOneSprites[counter].id;

        document.getElementById(currentElement).style.backgroundImage = `url("./src/assets/img/pokeSprites/${avatarOne.alt}.png")`;
        document.getElementById(currentElement).classList.add('backgroundGrids');
    };

    for (let counter = 0; counter < playerTwoSprites.length; counter++) {
        let currentElement = playerTwoSprites[counter].id;
        
        document.getElementById(currentElement).style.backgroundImage = `url("./src/assets/img/pokeSprites/${avatarTwo.alt}.png")`;
        document.getElementById(currentElement).classList.add('backgroundGrids');

    };
}, 1);
// --------------------------------------------------------------------
const turnSelection = turn => {
    if (turn === 1) {currentPlayer = nicknames[1]; actualTurn = 2;} else {currentPlayer = nicknames[0]; actualTurn = 1;};
};
let checkStart = setInterval(() => {
    if (gameCondition) {
        let actualTurn = Math.floor(Math.random() * 20) + 1;
        turnSelection(actualTurn);
        clearInterval(checkStart);        
    };
}, 100);
turnSelection();
// --------------------------------------------------------------------
// RULES SECTION
let button = document.querySelector('.rulesButton');
const showRules = () => {
        let rulesTable = document.createElement('div');
        
        rulesTable.classList.add('rulesDiv');
        rulesTable.style.opacity = '1';
        innerModal.appendChild(rulesTable);
        
        let exitButton = document.createElement('div');
        exitButton.innerText = 'X';
        exitButton.classList.add('exitButton');


        exitButton.addEventListener('click', function(){
            const deleted = document.querySelector('.rulesDiv');
            deleted.remove();
            button.removeAttribute('disabled');
        })

        button.setAttribute('disabled', '');

        rulesTable.appendChild(exitButton);
        
        
        let textRules = document.createElement('p');
        textRules.style.width = '90%'
        textRules.innerHTML += 'Connect Four - Rules<br><br>The two players alternate turns dropping one of their discs at a time into an unfilled column, until the second player, with red discs, achieves a diagonal four in a row, and wins the game. If the board fills up before either player achieves four in a row, then the game is a draw.'

        rulesTable.appendChild(textRules);
};
button.addEventListener('click', function(){showRules()});
// --------------------------------------------------------------------
const startGame = () => {
    modal.classList.add('mostrar');
    modal.addEventListener('click', (e) => {
        if(e.target.className === 'botaoIniciar') {
            const nick1 = document.querySelector('#nick1').value;
            const nick2 = document.querySelector('#nick2').value;
            
            if (nick1 === '' || nick1 === 'Type your nickname here!' || avatarOne === undefined) {
                alert('Missing Nickname or Pokémon!');
                return;
            };
            
            if (nick2 === '' || nick2 === 'Type your nickname here!' || avatarTwo === undefined) {
                alert('Missing Nickname or Pokémon!');
                return;
            };

            modal.classList.remove('mostrar');
            document.querySelector('.gameGrids').style.display = 'flex';
            document.querySelector('#playerScore').style.display = 'flex';

            nicknames.push(nick1, nick2);

            music.play();
            music.volume = 0.1;
            music.autoplay;

            gameCondition = true;

            return nicknames;
        }
    });
}; startGame();
// --------------------------------------------------------------------
// VICTORY AND DRAW
let vitoria = 'false'
const vitoriaColuna = () => {
    if(actualTurn === 1){
         
        for(let i = 0; i < arrPlay1.length; i++){
            
            let pos = parseInt(arrPlay1[i]);

            if(arrPlay1.includes(String(pos + 1))){
                pos++;
                if(arrPlay1.includes(String(pos + 1))){
                    pos++;
                    if(arrPlay1.includes(String(pos + 1))){
                        vitoria = true; 
                        declaraVitoria(); 
                        return;
                    }
                }
            }
        }
    }else if(actualTurn === 2){
        for(let i = 0; i < arrPlay2.length; i++){
            let pos = parseInt(arrPlay2[i]);
            
            if(arrPlay2.includes(String(pos + 1))){
                pos++;
                
                if(arrPlay2.includes(String(pos + 1))){
                    pos++;
                    if(arrPlay2.includes(String(pos + 1))){
                        vitoria = true; 
                        declaraVitoria(); 
                        return;
                    }
                }
            }
        }
    }
}
const vitoriaLinha = () => {
    if(actualTurn === 1){
        for(let i = 0; i < arrPlay1.length; i++){
            let pos = parseInt(arrPlay1[i]);
            if(arrPlay1.includes(String(pos + 10))){
                pos+=10;
                if(arrPlay1.includes(String(pos + 10))){
                    pos+=10;
                    if(arrPlay1.includes(String(pos + 10))){
                        vitoria = true; 
                        declaraVitoria(); 
                        return;
                    }
                }
            }
        }
    }else if(actualTurn === 2){
        for(let i = 0; i< arrPlay2.length; i++){
            let pos = parseInt(arrPlay2[i]);
            if(arrPlay2.includes(String(pos + 10))){
                pos+=10;
                if(arrPlay2.includes(String(pos + 10))){
                    pos+=10;
                    if(arrPlay2.includes(String(pos + 10))){
                        vitoria = true; 
                        declaraVitoria(); 
                        return;
                    }
                }
            }
        }
    }
}
const vitoriaDiagonalDireita = () => {
    if(actualTurn === 1){
        for(let i = 0; i < arrPlay1.length;i++){
            let pos = parseInt(arrPlay1[i]);
            if(arrPlay1.includes(String(pos + 9))){
                pos+=9;
                if(arrPlay1.includes(String(pos + 9))){
                     pos+=9;
                    if(arrPlay1.includes(String(pos + 9))){
                        vitoria = true; 
                        declaraVitoria();
                        return;
                    }
                }
            }
        }
    }else if(actualTurn === 2){
        for(let i = 0; i < arrPlay2.length; i++){
            let pos = parseInt(arrPlay2[i]);
            if(arrPlay2.includes(String(pos + 9))){
                pos+=9;
                if(arrPlay2.includes(String(pos + 9))){
                    pos+=9;;
                    if(arrPlay2.includes(String(pos + 9))){
                        vitoria = true;
                        declaraVitoria();
                        return;
                    }
                }
            }
        }
    }
}
const vitoriaDiagonalEsquerda = () => {
    if(actualTurn === 1){
        for(let i = 0; i < arrPlay1.length; i++){
            let pos = parseInt(arrPlay1[i]);
            if(arrPlay1.includes(String(pos + 11))){
                pos+=11;
                if(arrPlay1.includes(String(pos + 11))){
                    pos+=11;
                    if(arrPlay1.includes(String(pos + 11))){
                        vitoria = true;
                        declaraVitoria();
                        return;
                    }
                }
            }
        }
    } else if (actualTurn === 2){
        for(let i = 0; i < arrPlay2.length; i++){
            let pos = parseInt(arrPlay2[i]);
            if(arrPlay2.includes(String(pos + 11))){
                pos+=11;
                if(arrPlay2.includes(String(pos + 11))){
                    pos+=11;
                    if(arrPlay2.includes(String(pos + 11))){
                        vitoria = true;
                        declaraVitoria();
                        return;
                    }
                }
            }
        }
    }
}
const empate = ()=> {
    if(arrPlay2.length === 21){
        vitoria = false;
        declaraVitoria();
        return;
    }
}
const condVitoria = () => {
    vitoriaColuna();
    vitoriaDiagonalDireita();
    vitoriaDiagonalEsquerda();
    vitoriaLinha();
    empate();
    return;
}
const declaraVitoria = () => {
    if (vitoria === true && actualTurn === 1) {
        showVictory(currentPlayer);
    } else if (vitoria === true && actualTurn === 2) {
        showVictory(currentPlayer);
    } else {
        showVictory(false);
    }
}
let reseting = 0;
const showVictory = (winner) => {
    if (reseting === 1) {return};
    const title = document.querySelector('.mainTitle');
    const winnerPok = document.createElement('img');
    const back = document.createElement('div');

    back.classList.add('endBackground');
    modal.appendChild(back);

    winnerPok.classList.add('winGif');

    if(winner === false){
        title.innerText = `Empate`;
        winnerPok.setAttribute('src', './src/assets/img/pokeSprites/draw.gif');
    } else{
        if (winner === nicknames[0]) {
            winnerPok.setAttribute('src', `./src/assets/img/pokeSprites/${avatarOne.alt}.gif`);
        } else {
            winnerPok.setAttribute('src', `./src/assets/img/pokeSprites/${avatarTwo.alt}.gif`);
        }
        title.innerHTML = `Parabéns ${winner}!<br>Você venceu`;
    }
    
    modal.appendChild(winnerPok);
    modal.classList.add('mostrar');
    innerModal.style.display = "none";

    createButtonOfRestart();
    reseting++;
}
const resetGame = () => {
    location.reload();  
}
const getTheRestartCommand = (e) => {
    let currentTarget = e.target;
    
    if(currentTarget.id === 'restartButton'){
        resetGame();
    }

}
const createButtonOfRestart = () => {
    const restartGame = document.createElement('button');

    restartGame.innerText = "Restart game";
    restartGame.classList.add('botaoIniciar');
    restartGame.setAttribute('id', 'restartButton');

    modal.appendChild(restartGame);

    modal.addEventListener('click', getTheRestartCommand);
};